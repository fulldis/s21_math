#include "s21_math.h"

long double s21_asin(double x) {
  long double result = 0;
  if (s21_fabs(x) > 1) {
    result = S21_NAN;
  } else if (s21_fabs(x) == 1) {
    result = x * S21_PI / 2;
  } else {
    result = s21_atan(x / s21_sqrt(1 - (x * x)));
  }
  return result;
}

long double s21_acos(double x) { return S21_PI / 2.0 - s21_asin(x); }

int s21_abs(int x) {
  int result = 0;
  if (x > 0) {
    result = x;
  } else {
    result = -x;
  }
  return result;
}

long double s21_fabs(double x) {
  long double result = x;
  if (result != result) {
    result = S21_NAN;
  } else if (x < 0) {
    result = -x;
  }
  return result;
}

long double s21_exp(double x) {
  long double add_value = 1;
  long double series = 1;
  long double i = 1;

  if (s21_isnan(x)) {
    series = S21_NAN;
  } else if (x == S21_INF) {
    series = S21_INF;
  } else if (x == -S21_INF) {
    series = 0;
  } else if (x == 1) {
    series = S21_EXP;
  } else if (x < -15) {
    series = s21_exp(x + 15) * S21_EXP_15;
  } else {
    while (s21_fabs(add_value) > S21_SMALL_EPS) {
      add_value *= x / i;
      i += 1;
      series += add_value;
      if (series > S21_DBL_MAX) {
        series = S21_INF;
        break;
      }
    }
  }
  return series;
}

void s21_log_normalize(double *x, int *power) {
  *power = 0;
  while (*x > 10) {
    *power += 1;
    *x /= 10;
  }
  while (*x < 1) {
    *power -= 1;
    *x *= 10;
  }
}

long double s21_log(double x) {
  long double result = 0;  //  initialised with 0
  if (x == S21_INF) {
    result = S21_INF;
  } else if (x > 0) {
    int n = 0;  //  initialised with 0
    s21_log_normalize(&x, &n);
    long double yn = x - 1;
    long double yn1 = yn;
    do {
      yn = yn1;
      yn1 = yn + 2 * (x - s21_exp(yn)) / (x + s21_exp(yn));
    } while (s21_fabs(yn - yn1) > S21_SMALL_EPS);
    result = yn + n * S21_LOG10;
  } else if (x == 0) {
    result = -S21_INF;
  } else {
    result = S21_NAN;
  }

  return result;
}

long double s21_trunc(double x) {
  long double result = 0;  //  initialised with 0;
  if (x > 0) {
    result = s21_floor(x);
  } else {
    result = -s21_floor(-x);
  }
  return result;
}

long double s21_fmod(double x, double y) {
  long double result = S21_NAN;
  if (s21_isnan(x) || s21_isnan(y) || s21_fabs(x) == S21_INF) {
    result = S21_NAN;
  } else if (s21_fabs(y) == S21_INF) {
    result = x;
  } else if (y != 0) {
    result = (long double)x - s21_trunc(x / y) * (long double)y;
    if (result >= 0 && x < 0) result += y;
  }
  return result;
}

long double s21_ceil(double x) {
  long double result = (long long)x;
  if (x != x) {
    result = S21_NAN;
  } else if (x < -S21_FLT_MAX || x > S21_FLT_MAX) {
    result = x;
  } else if (x > 0. && s21_fabs(x - result) > 0.) {
    result += 1;
  }
  return result;
}

long double s21_floor(double x) {
  long double result = (long long)x;
  if (x != x) {
    result = S21_NAN;
  } else if (x < -S21_FLT_MAX || x > S21_FLT_MAX) {
    result = x;
  } else if (x < 0. && s21_fabs(x - result) > 0.) {
    result -= 1;
  }
  return result;
}

int s21_isnan(long double x) { return (x != x) ? 1 : 0; }

long double s21_int_pow(double base, long long exp) {
  long double result = base;
  if (exp == 0) {
    result = 1;
  } else if (exp == 1) {
    result = base;
  } else if (exp % 2) {
    result = base * s21_int_pow(base, exp - 1);
  } else if (result != 0) {
    result = s21_int_pow(base * base, exp / 2);
  }
  return result;
}

long double s21_pow(double base, double exp) {
  long double res = 0;
  long double int_exp = s21_floor(exp);
  if (exp == 0 || base == 1) {
    res = 1;
  } else if (s21_isnan(exp) || s21_isnan(base)) {
    res = S21_NAN;
  } else if (exp == S21_INF) {
    if (s21_fabs(base) == 1)
      res = 1;
    else if (s21_fabs(base) > 1)
      res = S21_INF;
    else
      res = 0;
  } else if (base == -S21_INF && exp != -S21_INF) {
    if (exp < 0)
      res = 0;
    else
      res = s21_int_pow(-1, s21_fabs(exp)) * S21_INF;
  } else if (base < 0 && s21_floor(exp) != exp) {
    res = S21_NAN;
  } else if (exp < 0) {
    if (base == 0)
      res = S21_INF;
    else
      res = 1 / s21_pow(base, -1 * exp);
  } else if (exp > S21_FLT_MAX || exp == int_exp) {
    res = s21_int_pow(base, (long long)exp);
  } else {
    res = s21_exp(s21_log(base) * (exp - int_exp)) * s21_int_pow(base, int_exp);
  }
  return res;
}

long double s21_cos(double x) {
  long double result = 0;
  if (s21_isnan(x) || s21_fabs(x) == S21_INF) {
    result = S21_NAN;
  } else if (x == 0) {
    result = 1;
  } else if (x < 0) {
    result = s21_cos(-x);
  } else if (x > S21_PI * 2) {
    result = s21_cos(s21_fmod(x, 2 * S21_PI));
  } else {
    result = 1;
    long double mul = -x * x;
    long double add = 1;
    for (int i = 1; i < 20; i++) {
      add = add * mul / (2 * i * (2 * i - 1));
      result += add;
    }
  }
  return result;
}

long double s21_sin(double x) {
  long double result = 0;
  if (s21_isnan(x) || s21_fabs(x) == S21_INF) {
    result = S21_NAN;
  } else if (x == 0) {
    result = 0;
  } else if (x < 0) {
    result = -1 * s21_sin(-1 * x);
  } else if (x > S21_PI * 2) {
    result = s21_sin(s21_fmod(x, 2 * S21_PI));
  } else {
    result = 1;
    long double mul = (x - S21_PI / 2);
    mul *= -mul;
    long double add = 1;
    for (int i = 1; i < 20; i++) {
      add = add * mul / (2 * i * (2 * i - 1));
      result += add;
    }
  }
  return result;
}
/*  DESCRIPTION
   The sqrt() function compute the non-negative square root of x.

    SPECIAL VALUES

   sqrt(x) returns a NaN and generates a domain error for x < 0.  */

long double s21_sqrt(double x) {
  long double result = S21_NAN;
  if (x > 0) {
    result = s21_pow(x, 0.5);
  } else if (x == 0) {
    result = 0;
  }
  return result;
}

long double s21_tan(double x) {
  long double result;
  if (x < 0) {
    result = -s21_tan(-x);
  } else if (s21_fabs(x) < 0.001) {
    result = x;
  } else if (x > S21_PI) {
    result = s21_tan(s21_fmod(x, S21_PI));
  } else if (x == S21_PI / 2) {
    result = 16331239353195370.0;
  } else {
    result = s21_sin(x) / s21_cos(x);
  }
  return result;
}

long double s21_strange_atan(double x) {
  long double coeffs[] = {1.0 / 2.0,   -1.0 / 4.0,   1.0 / 12.0,
                          -1.0 / 40.0, 1.0 / 48.0,   -1.0 / 112.0,
                          1.0 / 288.0, -1.0 / 320.0, 1.0 / 704.0};
  long double pow = 1;
  long double result = S21_PI / 4;
  for (int i = 0; i < 9; i++) {
    pow *= (x - 1);
    result += pow * coeffs[i];
  }
  return result;
}

long double s21_atan(double x) {
  long double result = 0;
  if (s21_isnan(x)) {
    result = S21_NAN;
  } else if (s21_fabs(x) == S21_INF) {
    result = (x < 0) ? -S21_PI / 2 : S21_PI / 2;
  } else if (x < 0) {
    result = -s21_atan(-x);
  } else if (x > 1) {
    result = S21_PI / 2 - s21_atan(1 / x);
  } else if (x < 0.95) {
    result = x;
    long double add = x;
    for (long long i = 1; i < 2000; i++) {
      add *= -x * x;
      result += add / (2 * i + 1);
    }
  } else {
    result = s21_strange_atan(x);
  }
  return result;
}
